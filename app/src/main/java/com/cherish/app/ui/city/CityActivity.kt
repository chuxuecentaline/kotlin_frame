package com.cherish.app.ui.city

import com.cherish.app.R
import com.cherish.common.ui.BaseActivity

/**
 * @author: cherish
 * @description: 城市
 * @date: 2019/5/28 16:23
 * @version: 2.0
 */
class CityActivity : BaseActivity() {
    override fun setContentId() = R.layout.activity_city
    override fun bindData() {

    }

    override fun injectListener() {

    }


}
