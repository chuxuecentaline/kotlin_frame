package com.cherish.common.entity

/**
 * @author: cherish
 * @description: 实体对象基类
 * @date: 2019/5/7 15:40
 * @version: 2.0
 */
 open class BaseEntity{
    var id: String? = null
}