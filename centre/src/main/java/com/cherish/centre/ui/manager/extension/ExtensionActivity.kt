package com.cherish.centre.ui.manager.extension

import com.cherish.centre.R
import com.cherish.common.ui.BaseActivity

/**
 * @author: cherish
 * @description: 授信
 * @date: 2019/5/28 13:34
 * @version: 2.0
 */
class ExtensionActivity : BaseActivity() {
    override fun setContentId()=R.layout.activity_extension
    override fun bindData() {

    }

    override fun injectListener() {

    }


}
