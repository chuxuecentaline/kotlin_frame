package com.cherish.shopcart.ui.voucher

import com.cherish.common.ui.BaseActivity
import com.cherish.shopcart.R

/**
 * @author: cherish
 * @description: 代金券
 * @date: 2019/5/28 13:34
 * @version: 2.0
 */
class VoucherActivity : BaseActivity() {
    override fun setContentId() = R.layout.activity_voucher

    override fun bindData() {
    }

    override fun injectListener() {
    }


}
